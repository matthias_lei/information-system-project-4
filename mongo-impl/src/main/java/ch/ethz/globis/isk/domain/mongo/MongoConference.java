package ch.ethz.globis.isk.domain.mongo;

import ch.ethz.globis.isk.domain.Conference;
import ch.ethz.globis.isk.domain.ConferenceEdition;
import ch.ethz.globis.isk.domain.Publication;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.beans.ConstructorProperties;
import java.util.HashSet;
import java.util.Set;

@Document(collection="Conference")
public class MongoConference implements Conference {

    @Id
    private String id;
    private String name;

    @DBRef(lazy = true)
    Set<ConferenceEdition> editions;

    //@PersistenceConstructor
    public MongoConference() {
        editions = new HashSet<>();
    }
    
    public MongoConference(String id, String name, Set<ConferenceEdition> editions){
    	setId(id);
    	setName(name);
    	setEditions(editions);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ConferenceEdition> getEditions() {
        return editions;
    }

    public void setEditions(Set<ConferenceEdition> editions) {
        this.editions = editions;
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Conference)) return false;

        Conference that = (Conference) o;

        if (!getId().equals(that.getId())) return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;

        return true;
    }
}