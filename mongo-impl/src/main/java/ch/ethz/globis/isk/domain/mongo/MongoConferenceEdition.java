package ch.ethz.globis.isk.domain.mongo;

import ch.ethz.globis.isk.domain.Conference;
import ch.ethz.globis.isk.domain.ConferenceEdition;
import ch.ethz.globis.isk.domain.Proceedings;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="ConferenceEdition")
public class MongoConferenceEdition implements ConferenceEdition {

    @Id
    private String id;

    private Integer year;

    @DBRef(lazy = false)
    private Conference conference;

    @DBRef(lazy = false)
    private Proceedings proceedings;

    //@PersistenceConstructor
    public MongoConferenceEdition() {
	}
    
    public MongoConferenceEdition(String id, Integer year, Conference conference, Proceedings proceedings) {
    	setId(id);
    	setYear(year);
    	setConference(conference);
    	setProceedings(proceedings);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Conference getConference() {
        return conference;
    }

    public void setConference(Conference conference) {
        this.conference = conference;
    }

    public Proceedings getProceedings() {
        return proceedings;
    }

    public void setProceedings(Proceedings proceedings) {
        this.proceedings = proceedings;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "ConferenceEdition{" +
                "id='" + getId() + '\'' +
                ", year=" + getYear() +
                '}';
    }

	@Override
	public boolean equals(Object o){
	    if (this == o) return true;
	    if (!(o instanceof ConferenceEdition)) return false;
	
	    ConferenceEdition that = (ConferenceEdition) o;
	
	    if (!getId().equals(that.getId())) return false;
	    //if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
	
	    return true;
	}
    
    
}
