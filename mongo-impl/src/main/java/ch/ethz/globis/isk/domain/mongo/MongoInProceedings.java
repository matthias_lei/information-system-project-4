package ch.ethz.globis.isk.domain.mongo;


import java.util.Set;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Proceedings;

@Document(collection="Publication")
public class MongoInProceedings extends MongoPublication implements InProceedings {

    private String note;
    private String pages;

    @DBRef(lazy = false)
    private Proceedings proceedings;

    //@PersistenceConstructor
    public MongoInProceedings(){ 	
    }
    
    public MongoInProceedings(String id, String title, String electronicEdition, int year, Set<Person> authors, Set<Person> editors, String note, String pages, Proceedings proceedings) {
    	super(id, title, electronicEdition, year, authors, editors);
    	setNote(note);
    	setPages(pages);
    	setProceedings(proceedings);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public Proceedings getProceedings() {
        return proceedings;
    }

    @Override
    public void setProceedings(Proceedings proceedings) {
        this.proceedings = proceedings;
    }

}
