package ch.ethz.globis.isk.domain.mongo;


import java.util.Set;

import ch.ethz.globis.isk.domain.Article;
import ch.ethz.globis.isk.domain.JournalEdition;
import ch.ethz.globis.isk.domain.Person;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="Publication")
public class MongoArticle extends MongoPublication implements Article {

    private String cdrom;
    private String pages;

    @DBRef (lazy = false)
    private JournalEdition journalEdition;

    public MongoArticle(String id, String title, String electronicEdition, int year, Set<Person> authors, Set<Person> editors, String cdrom, String pages, JournalEdition journalEdition) {
    	super(id,title, electronicEdition, year, authors, editors);
    	setCdrom(cdrom);
    	setPages(pages);
    	setJournalEdition(journalEdition);
    }

    //@PersistenceConstructor
    public MongoArticle() {
	}

	public String getCdrom() {
        return cdrom;
    }

    public void setCdrom(String cdrom) {
        this.cdrom = cdrom;
    }

    public JournalEdition getJournalEdition() {
        return journalEdition;
    }

    public void setJournalEdition(JournalEdition journalEdition) {
        this.journalEdition = journalEdition;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }
}