package ch.ethz.globis.isk.domain.mongo;

import ch.ethz.globis.isk.domain.Conference;
import ch.ethz.globis.isk.domain.Journal;
import ch.ethz.globis.isk.domain.JournalEdition;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;
@Document(collection="Journal")

public class MongoJournal implements Journal {

    @Id
    private String id;
    private String name;

    @DBRef(lazy = true)
    private Set<JournalEdition> editions;

    //@PersistenceConstructor
    public MongoJournal() {
        this.editions = new HashSet<>();
    }
    
    public MongoJournal (String id, String name, Set<JournalEdition> editions){
    	setId(id);
    	setName(name);
    	setEditions(editions);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<JournalEdition> getEditions() {
        return editions;
    }

    public void addEdition(JournalEdition edition) {
        editions.add(edition);
    }
    public void setEditions(Set<JournalEdition> editions) {
        this.editions = editions;
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Journal)) return false;

        Journal that = (Journal) o;

        if (!getId().equals(that.getId())) return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;

        return true;
    }
}