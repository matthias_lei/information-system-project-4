package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.ConferenceEdition;
import ch.ethz.globis.isk.domain.mongo.MongoConferenceEdition;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MongoConferenceEditionDao extends MongoDao<String, ConferenceEdition> implements ConferenceEditionDao {

    @Override
    protected Class<MongoConferenceEdition> getStoredClass() {
        return MongoConferenceEdition.class;
    }

    @Override
    public ConferenceEdition createEntity() {
        return new MongoConferenceEdition();
    }

    @Override
    public List<ConferenceEdition> findByConferenceOrderedByYear(String conferenceId) {
//        String findAuthorsQuery = "Select ce from ConferenceEdition ce JOIN ce.conference c " +
//                "WHERE c.id = :conferenceId ORDER BY ce.year ASC";
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter("conferenceId", conferenceId);
//        return query.getResultList();
    	
//    	BasicQuery query = new BasicQuery("{ ConferenceEdition.conference.id : { $eq : " + "\""+ conferenceId + "\"" +" } }");
    	BasicQuery query = new BasicQuery("{ \"conference.$id\" : " + "\""+ conferenceId + "\" }");
    	query.with(new Sort(Sort.Direction.ASC, "year"));
		return (List) mo.find(query, getStoredClass());
    }
}