package ch.ethz.globis.isk.domain.mongo;

import java.util.Set;

import ch.ethz.globis.isk.domain.MasterThesis;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.School;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Publication")
public class MongoMasterThesis extends MongoPublication implements MasterThesis {

    @DBRef (lazy = false)
    private School school;

    //@PersistenceConstructor
    public MongoMasterThesis(){
    	
    }
    
    public MongoMasterThesis(String id, String title, String electronicEdition, int year, Set<Person> authors, Set<Person> editors, School school) {
    	super(id, title, electronicEdition, year, authors, editors);
    	setSchool(school);
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}