package ch.ethz.globis.isk.domain.mongo;

import ch.ethz.globis.isk.domain.Article;
import ch.ethz.globis.isk.domain.Journal;
import ch.ethz.globis.isk.domain.JournalEdition;
import ch.ethz.globis.isk.domain.Publication;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Document(collection="JournalEdition")
public class MongoJournalEdition implements JournalEdition {

    @Id
    private String id;

    private String number;
    private String volume;
    private Integer year;

    @DBRef (lazy = false)
    private Journal journal;

    @DBRef (lazy = true)
    private Set<Article> publications;

    //@PersistenceConstructor
    public MongoJournalEdition() {
        publications = new HashSet<>();
    }
    
    public MongoJournalEdition(String id, String number, String volume, Integer year, Journal journal, Set<Article> publications){
    	setId(id);
    	setNumber(number);
    	setVolume(volume);
    	setYear(year);
    	setJournal(journal);
    	setPublications(publications);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addArticle(Article publication) {
        publications.add(publication);
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Set<Article> getPublications() {
        return publications;
    }

    public void setPublications(Set<Article> publications) {
        this.publications = publications;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "JournalEdition{" +
                "year=" + getYear() +
                ", volume='" +  getYear() + '\'' +
                ", number='" +  getYear() + '\'' +
                ", id='" +  getYear() + '\'' +
                '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JournalEdition)) return false;

        JournalEdition that = (JournalEdition) o;

        if (!getId().equals(that.getId())) return false;
        return true;
    }
}