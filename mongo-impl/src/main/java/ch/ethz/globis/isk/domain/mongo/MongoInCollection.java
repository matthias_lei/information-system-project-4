package ch.ethz.globis.isk.domain.mongo;

import java.util.Set;

import ch.ethz.globis.isk.domain.Book;
import ch.ethz.globis.isk.domain.InCollection;
import ch.ethz.globis.isk.domain.Person;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Publication")
public class MongoInCollection extends MongoPublication implements InCollection {

    private String note;
    private String pages;
   
    @DBRef(lazy = false)
    private Book parentPublication;

    //@PersistenceConstructor
    public MongoInCollection(){
    }
    
    public MongoInCollection(String id, String title, String electronicEdition, int year, Set<Person> authors, Set<Person> editors, String note, String pages, Book parentPublication) {
    	super(id, title, electronicEdition, year, authors, editors);
    	setNote(note);
    	setPages(pages);
    	setParentPublication(parentPublication);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public Book getParentPublication() {
        return parentPublication;
    }

    public void setParentPublication(Book book) {
        this.parentPublication = book;
    }
}