package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.JournalEdition;
import ch.ethz.globis.isk.domain.mongo.MongoJournalEdition;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MongoJournalEditionDao extends MongoDao<String, JournalEdition> implements JournalEditionDao {

    @Override
    protected Class<MongoJournalEdition> getStoredClass() {
        return MongoJournalEdition.class;
    }

    @Override
    public JournalEdition createEntity() {
        return new MongoJournalEdition();
    }

    @Override
    public List<JournalEdition> findByJournalIdOrdered(String journalId) {
//        String findAuthorsQuery = "Select je from JournalEdition je JOIN je.journal j " +
//                "WHERE j.id = :journalId ORDER BY je.year, je.volume, je.number ASC";
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter("journalId", journalId);
//        return query.getResultList();
    	System.out.println("{ \"journal.$id\" : " + "\""+ journalId + "\" }");
    	//BasicQuery query = new BasicQuery("{ JournalEdition.journal.$id : { $eq : " + "\"" + journalId + "\"" + " } }");
    	
    	BasicQuery query = new BasicQuery("{ \"journal.$id\" : " + "\""+ journalId + "\" }");
    	query.with(new Sort(Sort.Direction.ASC, "year"));
		return (List) mo.find(query, getStoredClass());
    }
}
