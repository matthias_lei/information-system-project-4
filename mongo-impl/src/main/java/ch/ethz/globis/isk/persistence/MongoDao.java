package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.DomainObject;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import ch.ethz.globis.isk.util.Order;
import ch.ethz.globis.isk.util.OrderFilter;
import ch.ethz.globis.isk.domain.mongo.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class MongoDao<K extends Serializable, T extends DomainObject> implements Dao<K, T> {

    @Autowired
    @Qualifier("mongoOperations")
    MongoOperations mo;

    public List<String> getSubtypes(){
    	ArrayList<String> list = new ArrayList<String>();
    	list.add(getStoredClass().toString().substring(6));
    	return list;
    }
    
    @Override
    public long countAllByFilter(Map<String, Filter> filterMap) {
        Query query = countQueryFromFilterMap(filterMap, null);
        return mo.count(query, getStoredClass());
    }

    @Override
    public long count() {
        Query query = countQueryFromFilterMap(null, null);
        return mo.count(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAll() {
    	Query query = selectQueryFromFilterMap(null, null);
        return mo.find(query, getStoredClass());
    }

    @Override
    public T findOne(K id) {
    	Map<String, Filter> filter = new HashMap<String, Filter>();
    	filter.put("_id", new Filter(Operator.EQUAL, id));
    	Query query = selectQueryFromFilterMap(filter, null);
    	return mo.findOne(query, getStoredClass());
    	//return mo.findById(id, getStoredClass());
    }

    @Override
    public T findOneByFilter(Map<String, Filter> filterMap) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
        return mo.findOne(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
        return mo.find(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap, int start, int size) {
    	Query query = selectQueryFromFilterMap(filterMap, null).skip(start).limit(size);
        return mo.find(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap,
                                       List<OrderFilter> orderList,
                                       int start, int size) {
//        CriteriaQuery<T> query = selectQueryFromFilterMap(filterMap, orderList);
//
//        return em.createQuery(query).setFirstResult(start).setMaxResults(size).getResultList();
    	Query query = selectQueryFromFilterMap(filterMap, orderList).skip(start).limit(size);
    	return mo.find(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap,
                                       List<OrderFilter> orderList) {
        Query query = selectQueryFromFilterMap(filterMap, orderList);
        return mo.find(query, getStoredClass());
    }

    @Override
    public <S extends T> Iterable<S> insert(Iterable<S> entities) {
        for(S entity : entities) {
            insert(entity);
        }
        return entities;
    }

    @Override
    public <S extends T> S insert(S entity) {
    	mo.save(entity);
    	//mo.insert(entity);
        return entity;
    }

    protected abstract <S extends T> Class<S> getStoredClass();

    protected List<T> queryByReferenceIdOrderByYear(String entity, String referenceName, String referenceId) {

    	//search in array elements
    	String q1 = "{" + referenceName + ": { $elemMatch : { $id: " + "\"" + referenceId + "\" } } }";
    	//search in fields
    	String q2 = "{" + referenceName + ".$id : " + "\"" + referenceId + "\" }";
    	String q = "{ $or : [" + q1 + ", " + q2 + "]}";
    	BasicQuery query = new BasicQuery(q);
    	query.with(new Sort(Sort.Direction.ASC, "year"));
    	try {
			return (List) mo.find(query, Class.forName("ch.ethz.globis.isk.domain.mongo.Mongo" + entity));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
    }

    private Query countQueryFromFilterMap(Map<String, Filter> filterMap,
                                                        List<OrderFilter> orderList) {
        return selectQueryFromFilterMap(filterMap, null);
    }

    private Query selectQueryFromFilterMap(Map<String, Filter> filterMap,
                                                      List<OrderFilter> orderList) {
    	
    	Criteria criteria = new Criteria();
    	Filter filter;
    	Query query = new Query();
    	
    	// add all the filters
    	if(filterMap != null){
	    	for(String s : filterMap.keySet()){
	    		filter = filterMap.get(s);
	    	
	    		if(filter.getOperator() == Operator.EQUAL){
	    			criteria = criteria.where(s).is(filter.getValue());
	    		} else {
	    			criteria = criteria.where(s).regex((String) filter.getValue());
	    		}
	    	}
    	}
    	
    	// add all the class contraints
    	Criteria classCriteria = new Criteria();
    	
    	for (String clazz : getSubtypes()){
    		Criteria subCriteria = new Criteria();
    		classCriteria = subCriteria.where("_class").is(clazz).orOperator(classCriteria);
    	}
    	
    	// add both Criteria
    	criteria.andOperator(classCriteria);
    	
    	query = new Query(criteria);
    	
    	// add all the order constraints
    	if(orderList != null){
	    	for(OrderFilter of : orderList){
	    		if(of.getOrder() == Order.ASC) {
	        		query = query.with(new Sort(Sort.Direction.ASC, of.getField()));
	    		} else {
	    			query = query.with(new Sort(Sort.Direction.DESC, of.getField()));
	    		}
	    	}
    	}
    	
    	return query; 
    }
}