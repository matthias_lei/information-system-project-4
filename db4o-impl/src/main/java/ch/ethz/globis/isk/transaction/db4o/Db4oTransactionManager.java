package ch.ethz.globis.isk.transaction.db4o;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import ch.ethz.globis.isk.service.cache.RequestResultCache;
import ch.ethz.globis.isk.transaction.CacheAwareTransactionManager;

import com.db4o.ObjectContainer;

@Component
public class Db4oTransactionManager extends CacheAwareTransactionManager {

	@Autowired
	private ObjectContainer oc;
	
	@Autowired
    RequestResultCache cache;
	
    @Override
    public void rollback() {
    	oc.rollback();
    }

    @Override
    public void begin() {
        //no need for explicit begin
    }

    @Override
    public void commit() {
    	oc.commit();
    }
}