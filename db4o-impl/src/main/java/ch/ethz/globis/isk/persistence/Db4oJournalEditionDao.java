package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.JournalEdition;
import ch.ethz.globis.isk.domain.db4o.Db4oJournalEdition;

import org.springframework.stereotype.Repository;

import com.db4o.query.Query;

import java.util.List;

@Repository
public class Db4oJournalEditionDao extends Db4oDao<String, JournalEdition> implements JournalEditionDao {

    @Override
    protected Class<Db4oJournalEdition> getStoredClass() {
        return Db4oJournalEdition.class;
    }

    @Override
    public JournalEdition createEntity() {
        return new Db4oJournalEdition();
    }

    @Override
    public List<JournalEdition> findByJournalIdOrdered(String journalId) {
    	Query query = oc.query();
    	query.constrain(getStoredClass());
    	query.descend("journal").descend("id").constrain(journalId);
    	query.descend("year").orderAscending();
    	query.descend("volume").orderAscending();
    	query.descend("number").orderAscending();
    	return query.execute();
//        String findAuthorsQuery = "Select je from JournalEdition je JOIN je.journal j " +
//                "WHERE j.id = :journalId ORDER BY je.year, je.volume, je.number ASC";
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter("journalId", journalId);
//        return query.getResultList();
    }
}
