package ch.ethz.globis.isk.domain.db4o;


import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.Proceedings;

public class Db4oInProceedings extends Db4oPublication implements InProceedings {
	private static Integer idCounter = 0;
	
    private String note;
    private String pages;

    private Proceedings proceedings;

    public Db4oInProceedings() {
        setId(idCounter.toString());
        idCounter++;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public Proceedings getProceedings() {
        return proceedings;
    }

    @Override
    public void setProceedings(Proceedings proceedings) {
        this.proceedings = proceedings;
    }

}
