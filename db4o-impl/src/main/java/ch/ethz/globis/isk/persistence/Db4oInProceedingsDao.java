package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.db4o.Db4oInProceedings;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class Db4oInProceedingsDao extends Db4oDao<String, InProceedings> implements InProceedingsDao {

    @Override
    public InProceedings findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public List<InProceedings> findByProceedingsIdOrderByYear(String proceedingsId) {
        return queryByReferenceIdOrderByYear("InProceedings", "proceedings", proceedingsId);
    }

    @Override
    protected Class<Db4oInProceedings> getStoredClass() {
        return Db4oInProceedings.class;
    }

    @Override
    public InProceedings createEntity() {
        return new Db4oInProceedings();
    }

    
}