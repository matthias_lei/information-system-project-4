package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.db4o.Db4oPerson;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class Db4oPersonDao extends Db4oDao<String, Person> implements PersonDao {

    @Override
    protected Class<Db4oPerson> getStoredClass() {
        return Db4oPerson.class;
    }

    @Override
    public Person createEntity() {
        return new Db4oPerson();
    }

    @Override
    public Person findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }
}