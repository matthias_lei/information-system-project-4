package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.Conference;
import ch.ethz.globis.isk.domain.db4o.Db4oConference;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class Db4oConferenceDao extends Db4oDao<String, Conference> implements ConferenceDao {

    @Autowired
    ProceedingsDao proceedingsDao;

    @Override
    protected Class<Db4oConference> getStoredClass() {
        return Db4oConference.class;
    }

    @Override
    public Conference createEntity() {
        return new Db4oConference();
    }

    @Override
    public Conference findOneByName(String name) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("name", new Filter(Operator.EQUAL, name));
        return findOneByFilter(filterMap);
    }
}